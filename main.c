/**
  ******************************************************************************
  * @file    USART/USART_TwoBoards/DataExchangeInterrupt/main.c 
  * @author  MCD Application Team
  * @version V1.4.0
  * @date    24-July-2014
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes */
#include "main.h"
#include "stdio.h"
#include "math.h"

/* Defines */
#define LED1                            GPIO_Pin_4              // Pin D12 B
#define LED2                            GPIO_Pin_5              // Pin D11 B
#define LED3                            GPIO_Pin_11             // Pin D10 A
#define LED4                            GPIO_Pin_8              // Pin D9  A
#define LED5                            GPIO_Pin_1              // Pin A1  A
#define LED6                            GPIO_Pin_3              // Pin A2  A
#define ADC_CONVERTED_DATA_BUFFER_SIZE   ((uint32_t)  32)       // Size of array aADCxConvertedData[] 
#define true                            1
#define false                           0

/* Variables */
_Bool dBnceFlag = 0;              // check if debounce is necessary
float calib = 0;                // adjusted calibration value
float value1[2] = {0};          // values for calibration 1
float value2[2] = {0};          // values for calibration 2
uint8_t adc_counter = 0;
float currentI = 0;             // calculated current from ADC
float temperatureVal = 0;       // calculated temperature from ADC
float LEDTemp = 0;              // temperature value for LED Control
float resistorValNtcR1 = 0;     // calculated NTC resistor value from ADC
float checkTempVal = 0;         // temperature check for debounce
float dBnceDiff = 0;			// difference between check and temp
uint8_t TxBuffer[] = "0";       // measured temperature to send
uint8_t RxBuffer [1] = "0";     // adjusted calibration to receive
uint8_t CheckBuff[] = "0";
uint8_t CmdBuffer [0x02] = {0x00, 0x01}; // Transaction Command, Number of byte 
                                         // to receive or to transmit
uint8_t AckBuffer [0x02] = {0x00, 0x00}; // Transaction Command, ACK command



/* IN / OUT Variables*/
__IO uint8_t RxIndex = 0x00;
__IO uint8_t TxIndex = 0x00;
__IO uint16_t  ADC1ConvertedValue = 0; 
__IO uint16_t  ADC1ConvertedVoltage = 0;
__IO JOYState_TypeDef PressedButton  = JOY_NONE;
__IO uint8_t UsartMode = USART_MODE_TRANSMITTER;
__IO uint8_t UsartTransactionType = USART_TRANSACTIONTYPE_CMD;
__IO uint32_t TimeOut = 0x00;  



/* Structure Typedefs */
GPIO_InitTypeDef    GPIO_InitStructureLED;
GPIO_InitTypeDef    GPIO_InitStructure;
ADC_InitTypeDef     ADC_InitStructure;



/* Private functions ---------------------------------------------------------*/
void LED_Ctrl();
void LED_Config(void);
static void USART_Config(void);
static void ADC_Config(void);
static void SysTickConfig(void);
static void USART_TransDeg(float temperatureVal);
static float ADC_CalcDeg();

// doing changes for test-branch in SourceTree

int main(void)
{
  LED_Config();
  ADC_Config();
  USART_Config();
  SysTickConfig();
  
  while (1)
  {
    /*Calculate Degree Value*/
    temperatureVal = ADC_CalcDeg();
    
    /*Cotnrol LEDs*/
    LED_Ctrl();    
    
    /*Send Degree Value to GUI*/
    USART_TransDeg(temperatureVal);
    
  }
}

void LED_Ctrl(void)
{
    /* Save tempratur value */
    LEDTemp = (int) temperatureVal;
    
    /* Set LEDs on 10� steps*/
    if(LEDTemp > 99 & LEDTemp < 161)
    {
      LEDTemp -= 100;
    }
    else if(LEDTemp > 160)
    {
      LEDTemp -= 170;
    }
    
    if(LEDTemp >= 10 && LEDTemp <= 60)
    {                                                        	// 0�C 
     GPIO_WriteBit(GPIOB, LED1, Bit_SET);                    	// LED 1 ON
    }
    else
    {
     GPIO_WriteBit(GPIOB, LED1, Bit_RESET);                     // LED 1 OFF
    }
    if(LEDTemp >= 20 && LEDTemp <= 60)
    {                                                           // 10�C
     GPIO_WriteBit(GPIOB, LED2, Bit_SET);                       // LED 2 ON
    }
    else
    {
     GPIO_WriteBit(GPIOB, LED2, Bit_RESET);                     // LED 2 OFF
    }
    if(LEDTemp >= 30 && LEDTemp <= 60)
    {                                                           // 20�C
      GPIO_WriteBit(GPIOA, LED3, Bit_SET);                      // LED 3 ON
    }
    else
    {
      GPIO_WriteBit(GPIOA, LED3, Bit_RESET);                    // LED 3 OFF
    }
    if(LEDTemp >=40 && LEDTemp <= 60)
    {                                                          	// 30�C
      GPIO_WriteBit(GPIOA, LED4, Bit_SET);                      // LED 4 ON
    }
    else
    {
      GPIO_WriteBit(GPIOA, LED4, Bit_RESET);                    // LED 4 OFF
    }
    if(LEDTemp >= 50 && LEDTemp <= 60)
    {                                                           // 40�C
      GPIO_WriteBit(GPIOA, LED5, Bit_SET);                      // LED 5 ON
    }
    else
    {
      GPIO_WriteBit(GPIOA, LED5, Bit_RESET);                    // LED 5 OFF
    }
    if(LEDTemp == 60)
    {                                                           // 50�C
     GPIO_WriteBit(GPIOA, LED6, Bit_SET);                       // LED 6 ON
    }
    else
    {
      GPIO_WriteBit(GPIOA, LED6, Bit_RESET);                    // LED 6 OFF
    }
}

void LED_Config(void){
  
  /* GPIOC Periph clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);	
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

  /* Configure GPIO in output mode */
  GPIO_InitStructureLED.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructureLED.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructureLED.GPIO_Pin = LED1 | LED2;
  GPIO_Init(GPIOB, &GPIO_InitStructureLED);
  GPIO_InitStructureLED.GPIO_Pin =LED3| LED4| LED5| LED6;
  GPIO_Init(GPIOA, &GPIO_InitStructureLED);
  
}

static float ADC_CalcDeg(void)
{
  /* Get ADC1 converted data */
  ADC1ConvertedValue = ADC_GetConversionValue(ADC1);
  
  /* Compute the voltage */
  ADC1ConvertedVoltage = (ADC1ConvertedValue *3300)/0xFFF;
  
  /* Calcualte Current Value */
  currentI =((4720-ADC1ConvertedVoltage)) / 11.8; 
  
  /* Calculate resistor Value */
  resistorValNtcR1 = 1000*ADC1ConvertedVoltage / currentI; 
  
  /*Calcualte temperature value*/

  adc_counter += 1 ;
  if(adc_counter == 10){                                                //Test: langsame Messungen = weniger St�rung
	adc_counter = 0;  	  
	temperatureVal =  ( (log(1000*23.6/resistorValNtcR1))/0.049 ); //temperatureVal =  ( (log(1000*23.6/resistorValNtcR1))/0.049 ) + (mKali*resistorValNtcR1+bKali);
  }

  if ((value1[1] != 0) && (value2[1] == 0)) checkTempVal -= 100;      // calibrate value 1
  if (value2[1] != 0)checkTempVal -= 170;     
	
	dBnceDiff = (temperatureVal - checkTempVal); 	// Temperatur geht hoch Wert positiv. Temperatur geht runter Wert negativ
    
	if((dBnceDiff < -0.4)||(dBnceDiff > 0.4))						// Temperatur geht runter. Differenz groe�er 0,4�C, dann Anpassen  
    {																// Temperatur geht hoch.   Differenz groe�er 0,4�C, dann Anpassen 
      checkTempVal = temperatureVal;		
    }
    
  //------------------------------------------------------------------------ 
  // calcualte  calibration status and check
  //------------------------------------------------------------------------
  if ((value1[1] != 0) && (value2[1] == 0)) temperatureVal += 100;      // calibrate value 1
  if (value2[1] != 0)temperatureVal += 170;                             // calibrate value 2
  //------------------------------------------------------------------------    
  if ((value1[1] != 0) && (value2[1] == 0)) checkTempVal += 100;      // calibrate value 1
  if (value2[1] != 0)checkTempVal += 170;                             // calibrate value 2
  //------------------------------------------------------------------------    
  
  
return checkTempVal;
}

static void USART_TransDeg(float temperatureVal)
{
    TxBuffer[0] = (uint8_t)temperatureVal;
    TxIndex = 0x00;
    RxIndex = 0x00;
    UsartTransactionType = USART_TRANSACTIONTYPE_CMD; 
    UsartMode = USART_MODE_TRANSMITTER;

    CmdBuffer[0x00] = CMD_SEL;
    CmdBuffer[0x01] = 0x01;//CMD_SEL_SIZE;         
    
/* USART in Mode Transmitter--------------------------------------------------*/ 
    if (CmdBuffer[0x00]!= 0x00)
    {              
      while ((TxIndex < 0x02)&&(TimeOut != 0x00)){ }
      while ((USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET)&&(TimeOut != 0x00)){ }
      while ((RxIndex < 0x02)&&(TimeOut != 0x00)){ }
      
      /* USART sends the data */
      UsartTransactionType = USART_TRANSACTIONTYPE_DATA; 
      TxIndex = 0x00;
      RxIndex = 0x00;
      
      /* Enable the USARTx transmit data register empty interrupt */
      USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);
      
      while ((TxIndex < GetVar_NbrOfData())&&(TimeOut != 0x00)){ }
      while ((USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET)&&(TimeOut != 0x00)){ }
    }
    CmdBuffer[0x00] = 0x00;
        
/* USART in Mode Receiver-----------------------------------------------------*/    
    while ((RxIndex < 0x02)&&(TimeOut != 0x00)){ }
    
    UsartMode = USART_MODE_RECEIVER;
    
    while ((TxIndex < 0x02)&&(TimeOut != 0x00)){ }
    
    /* The software must wait until TC=1. The TC flag remains cleared during all data
       transfers and it is set by hardware at the last frame�s end of transmission*/
    TimeOut = USER_TIMEOUT;
    
    while ((USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET)&&(TimeOut != 0x00)){ }
    
    /* USART receives the data */
    UsartTransactionType = USART_TRANSACTIONTYPE_DATA; 
    TxIndex = 0x00;
    RxIndex = 0x00;
          
    /* Set/Reset calibration once for Value 1 and 2*/
    if(USART_ReceiveData(USARTx) != calib)
    {
      calib = USART_ReceiveData(USARTx);
      
      /* Set Value 1 */
      if ((calib >=0) && (calib < 61)) 
      {
        value1[0] = calib;
        value1[1] = resistorValNtcR1;
      }

      /* Set Value 2 */
      if ((calib >=100) && (calib < 161))
      {
        value2[0] = (calib -100);
        value2[1] = resistorValNtcR1;
      }      

      /* Reset */
      if (calib == 255) 
      {
        value1[0] = 0;
        value1[1] = 0;
        value2[0] = 0;
        value2[1] = 0;
      }
    }
    
    while ((RxIndex < GetVar_NbrOfData())&&(TimeOut != 0x00)){ }
    
    CmdBuffer[0x00] = 0x00;
}

/**
  * @brief  Configures the USART Peripheral.
  * @param  None
  * @retval None
  */
static void ADC_Config(void)
{
/* GPIOC Periph clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
  /* ADC1 Periph clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
  
  /* Configure ADC Channel11 as analog input */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
    
  /* Initialize ADC structure */
  ADC_StructInit(&ADC_InitStructure);
  
  /* Configure the ADC1 in continous mode withe a resolutuion equal to 12 bits  */
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE; 
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;
  ADC_Init(ADC1, &ADC_InitStructure); 
  
  /* Convert the ADC1 Channel 11 with 239.5 Cycles as sampling time */ 
  ADC_ChannelConfig(ADC1, ADC_Channel_4 , ADC_SampleTime_239_5Cycles);   

  /* ADC Calibration */
  ADC_GetCalibrationFactor(ADC1);
  
  /* Enable ADCperipheral[PerIdx] */
  ADC_Cmd(ADC1, ENABLE);     
  
  /* Wait the ADCEN falg */
  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADEN)); 
  
  /* ADC1 regular Software Start Conv */ 
  ADC_StartOfConversion(ADC1);  
}
static void USART_Config(void)
{
  USART_InitTypeDef USART_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  
  /* Enable GPIO clock */
  RCC_AHBPeriphClockCmd(USARTx_TX_GPIO_CLK | USARTx_RX_GPIO_CLK, ENABLE);
  
  /* Enable USART clock */
  USARTx_APBPERIPHCLOCK(USARTx_CLK, ENABLE);
  
  /* Connect PXx to USARTx_Tx */
  GPIO_PinAFConfig(USARTx_TX_GPIO_PORT, USARTx_TX_SOURCE, USARTx_TX_AF);
  
  /* Connect PXx to USARTx_Rx */
  GPIO_PinAFConfig(USARTx_RX_GPIO_PORT, USARTx_RX_SOURCE, USARTx_RX_AF);
  
  /* Configure USART Tx and Rx as alternate function push-pull */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = USARTx_TX_PIN;
  GPIO_Init(USARTx_TX_GPIO_PORT, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Pin = USARTx_RX_PIN;
  GPIO_Init(USARTx_RX_GPIO_PORT, &GPIO_InitStructure);

  /* USARTx configuration ----------------------------------------------------*/
  /* USARTx configured as follow:
  - BaudRate = 19200 baud  
  - Word Length = 8 Bits
  - One Stop Bit
  - No parity
  - Hardware flow control disabled (RTS and CTS signals)
  - Receive and transmit enabled
  */
  USART_InitStructure.USART_BaudRate = 19200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USARTx, &USART_InitStructure);
  
  /* NVIC configuration */
  /* Enable the USARTx Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USARTx_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  /* Enable USART */
  USART_Cmd(USARTx, ENABLE);
}
/**
  * @brief  Configure a SysTick Base time to 10 ms.
  * @param  None
  * @retval None
  */
static void SysTickConfig(void)
{
  /* Setup SysTick Timer for 10ms interrupts  */
  if (SysTick_Config(SystemCoreClock / 5000))
  {
    /* Capture error */
    while (1);
  }
  /* Configure the SysTick handler priority */
  NVIC_SetPriority(SysTick_IRQn, 0x0);
}

uint8_t GetVar_NbrOfData(void)
{
  return CmdBuffer[0x01];
}
/**
  * @brief  Fills buffer.
  * @param  pBuffer: pointer on the Buffer to fill
  * @param  BufferLength: size of the buffer to fill
  * @retval None
  */
#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
